import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json


@Serializable
data class RequestData(val exampleField: String)

@Serializable
data class ResponseData(val resultField: String)

fun Application.module() {

    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
            isLenient = true
        })
    }

    routing {
        get("/orders") {
            call.respond(ResponseData("GET request handled"))
        }
        post("/orders") {
            val requestData = call.receive<RequestData>()
            call.respond(ResponseData("POST request handled, received data: ${requestData.exampleField}"))
        }
    }
}

    fun main() {
        embeddedServer(Netty, port = 8080, module = Application::module).start(wait = true)
    }
