// Sample of Service invocation in Dapr
// Reference https://docs.dapr.io/reference/api/service_invocation_api/

import kotlinx.serialization.Serializable
import io.ktor.client.HttpClient
import io.ktor.client.call.*
import io.ktor.client.engine.cio.CIO
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.http.ContentType
import io.ktor.http.contentType
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json

// Data class to represent the request structure
@Serializable
data class RequestData(val exampleField: String)

// Data class to represent the response structure
@Serializable
data class ResponseData(val resultField: String)

// Function to perform GET and POST requests to the target service
suspend fun performRequests() {
    // Initialize the HTTP client with ContentNegotiation plugin for handling JSON content
    val client = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json { isLenient = true; prettyPrint = true })
        }

        expectSuccess = true
    }

    // Define the URL for service invocation:
    // myapp - service name
    // orders - service method
    val url = "http://localhost:3500/v1.0/invoke/myapp/method/orders"

    try {
        // Perform a GET request and deserialize the response to ResponseData
        val getResponse: ResponseData = client.get(url).body()

        // Perform a POST request with RequestData serialized as JSON and response deserialized in postResponse
        val requestData = RequestData("newOrder")
        val postResponse: ResponseData = client.post(url) {
            contentType(ContentType.Application.Json)
            setBody(requestData)
        }.body()

        // Print the responses from GET and POST requests
        println("GET response: $getResponse")
        println("POST response: $postResponse")
    } catch (e: Exception) {
        println("Error occurred during the request: ${e.message}")
    } finally {
        client.close()
    }
}

suspend fun main() {
    performRequests()
}
