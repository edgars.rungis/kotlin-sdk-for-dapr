// Sample of message consumption from DAPR Pub/sub component; message is deserialized from JSON to String
// Reference https://docs.dapr.io/reference/api/pubsub_api/

import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.json.Json

// Class that manages subscriptions and routes for processing incoming messages
class SubscriptionHandler(private val application: Application, private val pubsubname: String) {
    // List of subscriptions
    private val subscriptions = mutableListOf<String>()

    // Function to add subscriptions and create routes to handle incoming messages
    fun addSubscription(topic: String, onMessageReceived: suspend (String) -> Unit) {
        // Create a route for the subscription
        val route = "/${topic.replace("/", "-")}"

        // If the subscription does not exist, add it
        if (!subscriptions.contains(topic)) {
            application.routing {
                post(route) {
                    // Deserialize the incoming message
                    val message = call.receive<String>()
                    // Call the provided callback function with the deserialized message
                    onMessageReceived(message)
                    // Respond with an HTTP status code of OK to remove message from topic
                    call.respond(HttpStatusCode.OK)
                }
            }
            // Add the subscription to the list
            subscriptions.add(topic)
        }
    }

    // Function to get the list of subscriptions in the expected format for Dapr runtime
    private fun getSubscriptions(): List<Map<String, String>> {
        return subscriptions.map { topic ->
            val route = "/${topic.replace("/", "-")}"
            mapOf(
                "pubsubname" to pubsubname,
                "topic" to topic,
                "route" to route
            )
        }
    }

    // Function to configure the server to handle subscription requests and JSON content negotiation
    private fun configureServer() {
        // Install ContentNegotiation plugin for handling JSON content
        application.install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                isLenient = true
            })
        }

        // Configure the route to handle subscription requests from Dapr sidecar
        application.routing {
            get("/dapr/subscribe") {
                call.respond(getSubscriptions())
            }
        }
    }

    // Initialize the SubscriptionHandler
    init {
        configureServer()
    }
}

fun main() {
    // Create and start an embedded server using the Netty engine on port 8080
    val server = embeddedServer(Netty, port = 8080) {

        // Instantiate the SubscriptionHandler with the pub-sub component name
        val subscriptionHandler = SubscriptionHandler(this, "pubsub")

        // Add a subscription to the "input" topic and provide a callback function to process messages
        subscriptionHandler.addSubscription("input") { message: String ->
            println("Message received: $message")
        }

    }
    // Start the server and wait for incoming requests
    server.start(wait = true)
}
