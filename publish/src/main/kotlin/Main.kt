// Sample of Pub/sub component where data is published to a given topic
// As a result, Dapr constructs a CloudEvent envelope with Data serialized to JSON
// Reference: https://docs.dapr.io/reference/api/pubsub_api/

import io.ktor.client.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json

// Data class to represent the message structure
@Serializable
data class Data(val field: String)

// Class to handle publishing messages to Dapr Pub/Sub component
class DaprPublisher(
    private val daprHost: String = "http://localhost:3500",
    private val pubSubComponentName: String
) {
    // Initialize the HTTP client with ContentNegotiation plugin for handling JSON content
    private val httpClient = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json { isLenient = true; prettyPrint = true })
        }
    }

    // Function to publish a message to a specific topic
    suspend fun publish(topicName: String, data: Data): Boolean {
        return try {
            // Make an HTTP POST request to the Dapr runtime with the topic and message
            val response: HttpResponse = httpClient.post("$daprHost/v1.0/publish/$pubSubComponentName/$topicName") {
                contentType(ContentType.Application.Json)
                setBody(data)
            }
            // If the response status is NoContent, the message was published successfully
            response.status == HttpStatusCode.NoContent
        } catch (e: Exception) {
            println("Error: ${e.message}")
            false
        }
    }

    // Function to close the HTTP client
    fun close() {
        httpClient.close()
    }
}

suspend fun main() {
    // Define the Pub/Sub component name and topic name
    val pubSubComponentName = "pubsub"
    val topicName = "input"

    // Create an instance of DaprPublisher
    val publisher = DaprPublisher(pubSubComponentName = pubSubComponentName)

    // Create a Data instance with the message content
    val data = Data("Hello, Dapr from Kotlin!")

    // Publish the message and print the result
    if (publisher.publish(topicName, data)) {
        println("Message published successfully")
    } else {
        println("Error publishing message")
    }

    // Close the publisher's HTTP client
    publisher.close()
}
