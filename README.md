# DAPR


## Getting started

To make it easy for you to get started with Dapr, here are instruvtions how to start. 


## Install Dapr on your local machine

You need to follow the step by step guide: https://docs.dapr.io/getting-started/install-dapr-cli. The recommended development environment requires Docker. While you can initialize Dapr without a dependency on Docker, the next steps in this guide assume the recommended Docker development environment.

1. Install Dapr CLI
2. Initialize Dapr in your local environment by using Docker

## Running provided examples

Provided examples are Gradle-based Kotlin projects and you will need to create a "fat" JAR file that contains all the project's dependencies and resources in a single JAR file in order to run it with Dapr:

```
./gradlew shadowJar
```

### Publish-Subscribe

This demo involves 2 projects - **pubsub** for subscribing to the topic and getting data stream and **publish** for showing publishing data to the topic. 

First run **pubsub** component which will subscribe to topic and print out messages to the screen. Both commands need to be run from the respective project directories. 

```
 dapr run --app-id  pubsub  --app-protocol http --app-port 8080  -- java -jar build/libs/pubsub-all.jar
```

Next run **publish** component which will publish message to the topic which will be consumed by **pubsub**

```
  dapr run --app-id publish --app-protocol http --dapr-http-port 3500   -- java -jar build/libs/publish-all.jar
```

### Invoke service in Dapr

This demo involves 2 projects - **invoke** and **respond** which demonstrates how to invoke service by using Dapr. 

Both commands need to be run from the respective project directories.

First run **respond** which will respond to the calls of the  **invoke** service:

```
  dapr run --app-id myapp  --app-protocol http --app-port 8080  -- java -jar build/libs/respond-all.jar
```

Next run **invoke** service which demonstrates how to invoke service in Dapr by using HTTP GET and HTTP POST:
```
  dapr run --app-id invoke --app-protocol http --dapr-http-port 3500   -- java -jar build/libs/invoke-all.jar
```
